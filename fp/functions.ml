let sign = (<=) 0;;

let absvalue n = if sign n then n else (-n);;

let max2 m n = if m > n then m else n;;

let rec fib n = if n < 2 then 1 else fib (n - 1) + fib (n - 2);;

print_int (fib 6);;
