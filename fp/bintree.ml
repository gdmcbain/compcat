type 'a binTree = Tip of 'a | Node of ('a binTree) * ('a binTree)

let rec breadth = function		(* Exercise 2.10.6.1 *)
    Tip _ -> 1
  | Node (a, b) -> (breadth a) + (breadth b)

let rec depth = function		(* Exercise 2.10.6.2 *)
    Tip _ -> 0
  | Node (a, b) -> 1 + max (depth a) (depth b)

let rec tips = function			(* Exercise 2.10.6.3 *)
    Tip x -> [x]
  | Node (a, b) -> List.append (tips a) (tips b)

let rec binTreeFold f ftip = function
    Tip x -> ftip x
  | Node (a, b) ->
      let f1 = binTreeFold f ftip in
      f (f1 a) (f1 b)
	
let rec breadthFold = binTreeFold (+) (fun _ -> 1)
let rec depthFold = binTreeFold (fun a b -> 1 + (max a b)) (fun _ -> 0)
let rec tipsFold = binTreeFold List.append (fun x -> [x])

let _ = begin
  let b = Node (Node(Tip 1, Tip 2), Tip 3) in
  (* let l = tipsFold b in *)
  (* List.iter (Printf.printf "%d ;") l *)
  print_int (breadthFold b)
end;
