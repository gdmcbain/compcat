(* Rydeheard & Burstall 1989, 2.10.4 *)

let compose g f = function x -> g (f x);;

