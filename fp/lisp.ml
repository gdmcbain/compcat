exception Empty_list

let rec list_max = function (* Exercise 2.10.5.1 *)
    [] -> raise Empty_list
  | [a] -> a
  | a::s -> let b = list_max s in if a > b then a else b

let rec sum = function (* Exercise 2.10.5.2 *)
    [] -> 0
  | a::s -> a + sum s

let rec horner c x = (* Exercise 2.10.5.3 *)
  match c with
    [] -> 0
  | a::s -> a + x * (horner s x)
	
let rec reverse_range = function
    0 -> []
  | n -> let m = (n - 1) in m :: reverse_range m

let rec reverse = function		(* Exercise 2.10.5.4. *)
    [] -> []
  | a::s -> List.append (reverse s) [a]

let rec maplist f = function (* Exercise 2.10.5.5 *)
    [] -> []
  | a::s -> (f a)::(maplist f s)

let rec fold f x = function (* Exercise 2.10.5.6 *)
    [] -> x
  | a::s -> (fold f (f a x) s)
	
let countdown = reverse_range 10;;	       

let countup = reverse countdown

let _ = begin
  print_int (sum (maplist (fun x -> x * x) countup));
  print_int (fold (+) 0 (maplist (fun x -> x * x) countup))
end;;
