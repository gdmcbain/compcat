(* Rydeheard & Burstall 1989, 2.10.3 *)

(* "In the text of the chapter we show how to define addition of
natural numbers.  Use this addition operation and the same cases as in
its defintion to define the multiplication of natural numbers." *)

type num = Zero | Succ of num;;

let rec numToInt = function
    Zero -> 0
  | Succ n -> 1 + (numToInt n)

let rec intToNum = function
    0 -> Zero
  | n -> Succ (intToNum (n - 1))

let rec add m = function
    Zero -> m
  | Succ p -> Succ (add m p)

let rec mul m = function
    Zero -> Zero
  | Succ p -> add m (mul m p)

let _ = print_int (numToInt (mul (intToNum 5) (intToNum 3)));;
