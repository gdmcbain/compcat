type rational = Rational of int * int;;

let numerator = function
    Rational (n, _) -> n

let denominator = function
    Rational (_, d) -> d

let rec gcd a = function (* Wikipedia "Greatest common divisor" *)
   0 -> abs a
 | b -> gcd b (a mod b)

let canonicalize (Rational (n, m)) =
  let d = gcd n m in
  let m1 = m / d in
  let s = if m1 > 0 then 1 else -1 in
  Rational (s * n / d, s * m1)
	
let add (Rational (a, b)) (Rational (c, d)) =
  canonicalize (Rational (d * a + c * b, b * d))

let negate (Rational (a, b)) = canonicalize (Rational (-a, b))

let sub r s = canonicalize (add r (negate s))

let mul (Rational (a, b)) (Rational (c, d)) =
  canonicalize (Rational (a * c, b * d))

let div (Rational (a, b)) (Rational (c, d)) =
  canonicalize (Rational (d * a, c * b))

let reciprocal (Rational (a, b)) = canonicalize (Rational (b, a))

let print_rational r = begin
  Printf.printf "%d / %d\n" (numerator r) (denominator r)
end;;

let fraction n d = Rational (n, d)
    
