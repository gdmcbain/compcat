type fractional = Fractional of int list * int list

let rec decimalize = function
    [] -> 0.0
  | d::s -> 0.1 *. ((float d) +. (decimalize s))

let rec canonicalize ds =
  let rs = List.rev ds in
  let divmod dividend divisor = (dividend / divisor, dividend mod divisor) in
  let divmod10 n = divmod n 10 in
  List.rev (List.map divmod10 rs)
                    
  
let _ = begin
   print_float (decimalize []); print_endline "";
   print_float (decimalize [1]); print_endline "";
   print_float (decimalize [1; 4; 1; 5; 9; 2; 6]); print_endline "";
   print_endline ""               
  end
