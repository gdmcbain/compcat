type rational

val add : rational -> rational -> rational
val negate : rational -> rational
val sub : rational -> rational -> rational
val mul : rational -> rational -> rational
val div : rational -> rational -> rational
val reciprocal : rational -> rational

val print_rational : rational -> unit    

val fraction : int -> int -> rational
